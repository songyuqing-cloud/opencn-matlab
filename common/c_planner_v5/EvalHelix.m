function [r0D, r1D, r2D, r3D] = EvalHelix(CurvStruct, u_vec)
if ~coder.target('MATLAB')
coder.inline('never')
coder.ceval('ZoneScopedN', coder.opaque('const char*', '"EvalHelix"'));
end
%
P0      = CurvStruct.P0;
P1      = CurvStruct.P1;
evec    = CurvStruct.evec;
theta   = CurvStruct.theta;
pitch   = CurvStruct.pitch;
N       = length(u_vec);
%
P0P1    = P1 - P0;
EcrP0P1 = cross(evec, P0P1);

r0D = zeros(3, N);
r1D = zeros(3, N);
r2D = zeros(3, N);
r3D = zeros(3, N);


if ~c_assert(MyNorm(EcrP0P1) > eps, 'e cross P0P1 = 0')
    return;
end

% if pitch == 0
%     if ~c_assert(evec'*P0P1 > eps, 'e'' * P0P1 = 0')
%         return;
%     end
% end
%
C           = CurvStruct.CorrectedHelixCenter;
CP0         = P0 - C;
phi_vec     = theta*u_vec;
EcrCP0      = cross(evec, CP0);
cphi        = mycos(phi_vec);
sphi        = mysin(phi_vec);
%

cphiTCP0    = CP0 * cphi;
sphiTCP0    = CP0 * sphi;
cphiTEcrCP0 = EcrCP0 * cphi;
sphiTEcrCP0 = EcrCP0 * sphi;
Sign        = sign(P0P1'*evec);
%
r0D       = bsxfun(@plus, C, cphiTCP0  + sphiTEcrCP0  + pitch/(2*pi)*evec*phi_vec);
r1D       = bsxfun(@plus, -theta  *sphiTCP0  + theta  *cphiTEcrCP0, theta * pitch/(2*pi) * evec);
r2D       = -theta^2*cphiTCP0  - theta^2*sphiTEcrCP0;
r3D       =  theta^3*sphiTCP0  - theta^3*cphiTEcrCP0;

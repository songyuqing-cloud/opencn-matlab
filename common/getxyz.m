function [x,y,z] = getxyz(r0D, i)
    [x,y,z] = r0D(:, i);
end